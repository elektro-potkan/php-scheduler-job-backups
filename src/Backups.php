<?php

declare(strict_types=1);

namespace ElektroPotkan\Scheduler\Jobs;

use ElektroPotkan\Backups\IManager as IBackupManager;
use Nette;


/**
 * Backups scheduled job
 */
class Backups extends AExpression {
	use Nette\SmartObject;
	
	
	/** @var IBackupManager */
	private $bak;
	
	
	/**
	 * Constructor
	 */
	public function __construct(string $cron, IBackupManager $bak){
		parent::__construct($cron);
		
		$this->bak = $bak;
	} // constructor
	
	public function run(): void {
		$this->bak->create();
		$this->bak->purge();
	} // run
} // class Backups
