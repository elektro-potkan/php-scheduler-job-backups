Scheduler - Backups Job
=======================

Scheduler job for creating and purging backups (using the `elektro-potkan/backups` package).
This package provides the `Backups` job for use with the main `elektro-potkan/scheduler` package.


Usage
-----
```php
// $backupManager is an instance implementing ElektroPotkan\Backups\IManager
$job = new ElektroPotkan\Scheduler\Jobs\Backups('0 0 * * *', $backupManager);// run daily at midnight

// register the job ($scheduler is an instance implementing ElektroPotkan\Scheduler\IScheduler)
$scheduler->add($job);
```

### Using Nette DI
```neon
scheduler:
	jobs:
		- ElektroPotkan\Scheduler\Jobs\Backups('0 0 * * *')
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
